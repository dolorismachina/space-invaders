﻿using GCL;
using SFML.Graphics;
using SFML.Window;
using SFML.Audio;
using System;
using System.Collections.Generic;

namespace SpaceInvaders
{
    public class Player : GameSprite
    {
        public Player(Vector2f position, Texture texture)
            : base(position,texture)
        {
            Health = 100;
            Points = 0;
            RateOfFire = 10;
            ShieldActive = true;
        }

        public int Health { get; set; }
        public int Points { get; set; }
        public float RateOfFire { get; set; }
        public bool ShieldActive { get; set; }

        public void Shoot(List<Projectile> projectiles)
        {
            activeWeapon.Shoot(projectiles, this);
        }

        public Weapon activeWeapon = new Weapon();
    }
}
