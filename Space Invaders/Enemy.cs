﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvaders
{
    class Enemy : GameSprite
    {
        public Enemy(Vector2f position, Texture texture)
            : base(position,texture)
        {
            Velocity = 150;
        }

        public Vector2f TargetPosition { get; set; }
        public Vector2f DirectionVector { get; set; }
        public static float Velocity { get; set; }
    }
}
