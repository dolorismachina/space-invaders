﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceInvaders
{
    class Powerup : GameSprite
    {
        public Powerup(Vector2f position, Texture texture)
            : base(position, texture) { }
    }
}
